<?php
/**
 * Controls the game functions
 *
 * 
 * @package  app
 * @extends  Base_Manager
 */
class Controller_Game extends Base_Manager
{
	/**
	 * Builds the game and the users board
	 * @param  string $crypt Hash of the game the user is playing
	 * @return void
	 */
	public function action_play($crypt)
	{
		// Add the games javascript and jQuery
		$this->template->js = array("https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js", 'game.js');

		// Decrypt the hash to get the game's id
		$game_id = Crypt::decode($crypt);
		
		// Get teh game and the type of game (for the board).
		$game = Model_Game::query()->where('id', $game_id)->related('parent')->get_one();

		// Find the players playing, the type, and the user's name.
		$data['players'] = json_decode($game->players, true);
		$data['type'] = $game->parent->name;
		$data['username'] = Session::get('username');

		// If the user already has a board made for this game, get it.
		// (This can probably be done with localstorage now in js)
		if($configuration = Session::get('board'))
		{
			$configuration = unserialize($configuration);
			$data['squares'] = Model_Square::query();
			foreach ($configuration as $square) {
				$data['squares']->or_where('id', $square);
			}
			$data['squares'] = $data['squares']->get();
		}
		// If they don't have a board, make one
		else
		{
			// Get 24 random squares
			$data['squares'] = Model_Square::query()->where('type', $game->type)->order_by(DB::expr('RAND()'))->limit(24)->get();
			$configuration = array();
			foreach($data['squares'] as $square)
			{
				$configuration[] = $square->id;
			}
			$configuration = serialize($configuration);
			Session::set('board', $configuration);
			Session::set('moves', array());
		}

		// Find the moves that have been made
		$data['moves'] = Session::get('moves');
		$this->template->title = 'Game &raquo; Index';
		$this->template->content = View::forge('game/index', $data);
	}
	/**
	 * Creates a game
	 * 
	 * @param  string $type The type of game to be made
	 * @return void
	 */
	public function action_create($type)
	{
		// We don't need no template for this
		$this->template = null;
		$game = Model_Game::forge();
		$players = array();
		$players[] = Session::get('username');
		$game->players = json_encode($players);
		$game->creator = Model_User::query()->where('login_hash', Session::get('login_hash'))->get_one()->id;
		$game->type = $type;
		$game->save();

		// Create the hash for the game itself
		$crypt = Crypt::encode($game->id);
		// Save the hash for.....some....reason?
		Session::set('game', $crypt);
		Response::redirect('/game/play/'.$crypt);
	}

	/**
	 * Join a player to the game
	 * 
	 * @param  string $crypt The game's hash
	 * @return void
	 */
	public function action_join($crypt)
	{
		// No template
		$this->template = null;
		$game_id = Crypt::decode($crypt);
		$game = Model_Game::find_by_id($game_id);
		Session::set('game', $crypt);

		// Get the players and add the player to the username (not to mention let the other players know).
		$players = json_decode($game->players, true);
		$players[] = Session::get('username');
		$game->players = json_encode($players);
		$game->chat = $this->add_chat($game->chat, "<p class='action'>".Session::get('username')." has joined the game.</p>");
		$game->save();
		Response::redirect('/game/play/'.$crypt);
	}
	/**
	 * Record when someone clicks on a square
	 * @param  string $crypt Game hash
	 * @return void
	 */
	public function action_move($crypt)
	{
		$this->template = null;
		$game = Model_Game::find_by_id(Crypt::decode($crypt));
		$moves = json_decode($game->moves, true);
		$moves[] = Input::post('square');
		$game->moves = json_encode((array)$moves);
		$game->save();
	}
	/**
	 * Record when a user clicks on an already clicked square
	 * @param  string $crypt Game hash
	 * @return void
	 */
	public function action_unmove($crypt)
	{
		$this->template = null;
		$game = Model_Game::find_by_id(Crypt::decode($crypt));
		$moves = json_decode($game->moves, true);
		if (($key = array_search(Input::post('square'), $moves)) !== false) {
		    unset($moves[$key]);
		}
		$game->moves = json_encode($moves);
		$game->save();
	}
	/**
	 * Updates all feilds for use with the javascript heartbeat
	 * @param  [type] $crypt [description]
	 * @return [type]        [description]
	 */
	public function action_update($crypt)
	{
		$game = Model_Game::find_by_id(Crypt::decode($crypt));
		$return['players'] = json_decode($game->players, true);
		$return['moves'] = json_decode($game->moves, true);
		$return['winner'] = ($game->winner) ? $game->winner : false;
		$return['chat'] = ($game->chat) ? unserialize($game->chat) : false;
		return json_encode($return);
	}
	/**
	 * Removes the user from the game.
	 * @param  string $crypt Game hash
	 * @return void
	 */
	public function action_leave($crypt)
	{
		$game = Model_Game::find_by_id(Crypt::decode($crypt));
		$players = json_decode($game->players, true);
		if (($key = array_search(Session::get('username'), $players)) !== false) {
		    unset($players[$key]);
		}
		$game->players = json_encode((array)$players);
		$game->chat = $this->add_chat($game->chat, "<p class='action'>".Session::get('username')." has left.</p>");
		$game->save();
		Response::redirect('/type/'.$game->type);
	}
	/**
	 * Set a winner for the game
	 * @param  string $crypt Game hash
	 * @return void
	 */
	public function action_winner($crypt)
	{
		$this->template = null;
		$game = Model_Game::find_by_id(Crypt::decode($crypt));
		$game->winner = Session::get('username');
		$game->save();
	}
	/**
	 * Grabs the chat for the game (killing all html in it).
	 * @param  [type] $crypt [description]
	 * @return [type]        [description]
	 */
	public function action_chat($crypt)
	{
		$this->template = null;
		$game = Model_Game::find_by_id(Crypt::decode($crypt));
		$game->chat = $this->add_chat($game->chat, "<p><strong>".Session::get('username').": </strong> ".htmlspecialchars(Input::post('text'))."</p>");
		$chat = unserialize($game->chat);
		$game->save();

		echo json_encode($chat);
	}
	/**
	 * Add a message to the serialized chat array
	 * @param string $chat   The chat so far.
	 * @param string $string String to be added to the chat.
	 */
	private function add_chat($chat, $string)
	{
		$chatArray = unserialize($chat);
		$chatArray[] = $string;
		$return = serialize($chatArray);
		return $return;
	}
}
