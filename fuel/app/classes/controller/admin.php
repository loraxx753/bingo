<?php
/**
 * Controls all admin functions
 *
 * 
 * @package  app
 * @extends  Controller_Template
 */
class Controller_Admin extends Base_Admin
{
	/**
	 * Admin main access panel.
	 * @return void
	 */
	public function action_index()
	{
		\Config::load('simpleauth');
		$data['groups'] = \Config::get('groups');
		$data['catagories'] = Model_Category::find('all');
		$data['users'] = Model_User::find('all');

		$this->template->js = array("https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js", 'admin.js');
		$this->template->title = "Admin Panel";
		$this->template->content = View::forge('admin/index', $data);
	}

	/**
	 * Adds and deletes categories
	 * @param  string $action The action to take
	 * @param  int    $id     The id of the category to affect
	 * @return void
	 */
	public function action_category($action, $id = null)
	{
		switch($action)
		{
			case "add":
				$this->add_category();
				break;
			case "delete":
				$this->delete_category($id);
				break;
		}
		Response::redirect('/admin');
	}

	/**
	 * Adds a category to the database
	 */
	private function add_category()
	{
		$name = Input::post();
		$category = Model_Category::forge();
		$category->name = $name;
		try 
		{
			$category->save();
		}
		catch(Exception $e)
		{
			Session::set_flash('error', array("Problem saving the category!"));
			return;
		}
		Session::set_flash('success', array("Category has been added."));
		return;
	}

	/**
	 * Deletes a category from the database.
	 * @param  int $id Category ID
	 * @return void
	 */
	private function delete_category($id)
	{
		$category = Model_Category::find_by_id($id);

		try 
		{
			$category->delete();
		}
		catch(Exception $e)
		{
			Session::set_flash('error', array("Problem deleting the category!"));
			return;
		}
		Session::set_flash('success', array("Category has been deleted."));
		return;
	}
}