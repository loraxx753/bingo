<?php
/**
 * Base Admin Controller to make sure whoever is singned in is an administrator
 *
 * 
 * @package  app
 * @extends  Controller_Template
 */
class Base_Admin extends Controller_Template
{
	/**
	 * Runs before the page has a chance to load
	 * @return [type] [description]
	 */
    public function before()
    {
        parent::before(); // Without this line, templating won't work!

        // If the user is singed in.
		if(!Session::get('login_hash'))
		{
			Response::redirect('/user/login');
		}

	}

	public function router($resource, $arguments) {
		// And if the user isn't an administrator (level 100), then forbid their ass.
		if(!Auth::member(100))
		{
			$response = new Response();
			$response->set_header("HTTP/1.1", "403 Forbidden");
			return $response;
		}
		// The API key is valid, Route the request!
		parent::router($resource, $arguments);
	}	
}
