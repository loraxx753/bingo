<?php
/**
 * Base Controller to make sure people are signed in
 *
 * 
 * @package  app
 * @extends  Controller_Template
 */
class Base extends Controller_Template
{
	/**
	 * Runs before any page has a chance to load
	 * @return void
	 */
    public function before()
    {
        parent::before(); // Without this line, templating won't work!

        // Checks to make sure the user is logged in
		if(!Session::get('login_hash'))
		{
			Response::redirect('/user/login');
		}
	}
}
