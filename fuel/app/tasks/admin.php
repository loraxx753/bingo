<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.0
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2012 Fuel Development Team
 * @link       http://fuelphp.com
 */

namespace Fuel\Tasks;

/**
 * Robot example task
 *
 * Ruthlessly stolen from the beareded Canadian sexy symbol:
 *
 *		Derek Allard: http://derekallard.com/
 *
 * @package		Fuel
 * @version		1.0
 * @author		Phil Sturgeon
 */

class Admin
{

	/**
	 * Promotes or demotes a given user
	 * @param  string $username User to change
	 * @param  string $role     New role to give user (banned, user, moderator, or admin)
	 * @return string           Conformation or error message
	 */
	public static function permission($username, $role)
	{
		$roles = array('banned' => -1, 'user' => 1, 'moderator' => 50, 'admin' => 100);

		if(isset($roles[$role]))
		{
			$user = \Model_User::query()->where('username', $username)->get_one();
			if($user)
			{
				$user->group_id = $roles[$role];
				$user->save();

				return \Cli::color("$username has been changed to ".ucwords($role), "green");
			}
			return \Cli::color($username ." cannot be found.", 'red');
		}

		return \Cli::color("The ".ucwords($role) . " permission cannot be found.", "red");			

	}
}
