<?php
/**
 * The production database settings.
 */

return array(
	'default' => array(
		'connection'  => array(
			'dsn'        => 'mysql:host=localhost;dbname=bingo',
			'username'   => 'root',
			'password'   => 'root',
		),
	),
);
