<?php

namespace Fuel\Migrations;

class Fill_more_squares
{
	public function up()
	{
		$query = \DB::insert('squares');

		$query->columns(array(
			'value',
			'type',
			'created_at'));

		$values = array(
			'Both Hands in Pockets',
			'One Hand in Pocket for More Than 30 Seconds',
			'Mentions How Much He\'s Worth',
			'Checks Cellphone',
			'Makes Fun of IE',
		);
		
		foreach ($values as $value) {
			$query->values(array($value, 1, time()));
		}
			$query->execute();


	}

	public function down()
	{

	}
}