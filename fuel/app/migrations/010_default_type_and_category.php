<?php

namespace Fuel\Migrations;

class Default_type_and_category
{
	public function up()
	{
		$query = \DB::insert('categories');

		$query->columns(array(
			'id',
			'name',
			'created_at'));

		$query->values(array(1, 'Digital Media', time()));
		
		$query->execute();


		$query = \DB::insert('types');

		$query->columns(array(
			'id',
			'name',
			'category',
			'created_at'));

		$query->values(array(1, 'Novatnak', 1, time()));
		
		$query->execute();


	}

	public function down()
	{

	}
}