# Run apt-get update
execute "update" do
  command "apt-get update"
  user "root"
end

# install LAMP setup
include_recipe "apache2"
include_recipe "mysql::server"
include_recipe "php"
include_recipe "php::module_mysql"
include_recipe "apache2::mod_php5"
include_recipe "apache2::mod_rewrite"

# Use this to make the databases set in your VagrantFile!
node[:db].each do |name|
  execute "create database #{name}" do
    command "mysql -uroot -p#{node[:mysql][:server_root_password]} -e 'create database if not exists #{name}'"
    user "vagrant"
  end
end

# Want to setup a VirtualHost? Use the code below
web_app "bingo" do
  template "config/bingo.conf.erb"
  docroot "/mnt/bingo/public"
  server_aliases "bingo.local"
end

# What about phpmyadmin?
package "phpmyadmin" do
  action :install
end

execute "php oil r migrate" do
  command "php /mnt/bingo/oil r migrate"
end

execute "chmod -R 0777 /mnt/bingo/fuel/app/logs" do 
  command "chmod -R 0777 /mnt/bingo/fuel/app/logs"
end

# execute "php oil r user:register_admin test test test@test.com" do
#   command "php /mnt/snazzyman/oil r user:register_admin test test test@test.com"
# end
