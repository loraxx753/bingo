/**
 * checks the server to see if there's any new chat messages, players, or moves
 */
function heartbeat() {
	hash = document.URL.split('/').pop();
	$.post('/game/update/'+hash, function(data) {
		lis = '';
		for(i in data.players)
		{
			lis += "<li>"+data.players[i]+"</li>";
		}
		$('#players').html(lis);
		$('.selected').removeClass('selected');
		for(i in data.moves)
		{
			if($('td[data-square='+data.moves[i]+']').length > 0)
			{
				var td = $('td[data-square='+data.moves[i]+']');
				row = td.data('row');
				column = td.data('column');
				td.addClass('selected');
				chosen[row][column] = 1;
			}
		}
		// If the winner hasn't been determined, but your board has won. You win.
		if(determined == false && data.winner != false)
		{
			determined = true;
			if(username != data.winner)
			{
				end_game(data.winner+" won!");
			}
			else
			{
				end_game("You are the winner!");
			}
		}
		if(data.chat != false)
		{		
			var scrolled = $("#chat").get(0).scrollHeight - $("#chat").scrollTop();
			if(prev_chat.length < data.chat.length && (scrolled < 1 || $("#chat").get(0).scrollHeight == $("#chat").height()))
			{
				$('#chat').html(data.chat.join(''));
				$("#chat").animate({ scrollTop: $("#chat")[0].scrollHeight });
			}
		}

	}, 'json');
}
/**
 * Checks for a win based on row.
 */
function row_check() {
	win = true;
	for(i in chosen)
	{
		win = true;
		for(j in chosen[i])
		{
			if(chosen[i][j] == 0) {
				win = false;
			}
		}
		if(win == true) { break; }
	}
	return win;
}
/**
 * Checks for a diagonal right win
 */
function diagnoal_check_right() {
	win = true;
	for(i = 0; i < chosen.length; i++)
	{
		if(chosen[i][i] == 0)
		{
			win = false;
		}
	}
	return win;
}
/**
 * Checks for a vertiacal win
 */
function column_check() {
	win = true;
	for(i = 0; i < chosen.length; i++)
	{
		win=true;
		for(j = 0; j < chosen[i].length; j++)
		{
			if(chosen[j][i] == 0)
			{
				win = false;
			}
		}
		if(win == true) { break; }
	}
	return win;
}
/**
 * Checks for a diagonal left win
 */
function diagnoal_check_left() {
	win = true;
	for(i = chosen.length-1; i >= 0; i--)
	{
		if(chosen[chosen.length - 1 - i][i] == 0)
		{
			win = false;
		}
	}
	return win;
}
/**
 * Parent check for a winner
 */
function win_check() {
	if(row_check() || column_check() || diagnoal_check_left() || diagnoal_check_right())
	{
		hash = document.URL.split('/').pop();
		winner = true;
		$.post('/game/winner/'+hash, { 'winner' : username }, function() {
			end_game('You are the winner!');
		});
	}
}

/**
 * End the game
 * @param  string wintext Text to display
 * @return void
 */
function end_game(wintext){
	$('table').before("<p class='wintext'>"+wintext+"</p>");
	$('td').unbind('click');
}
/**
 * Board array
 * @type array
 */
chosen = Array(
	Array(0,0,0,0,0), 
	Array(0,0,0,0,0), 
	Array(0,0,0,0,0),
	Array(0,0,0,0,0), 
	Array(0,0,0,0,0)
);
determined = false;
/**
 * A log of the previous chat
 * @type array
 */
var prev_chat = Array();

/**
 * If someone hits enter in the chat, add that to the chat array
 */
function chat(e) {
	if(e.which == 13)
	{
		$('#chat_input').off('keypress.chat');

		$this = $(this);
		text = $(this).val();
		$this.val('');
		hash = document.URL.split('/').pop();
		$.post('/game/chat/'+hash, {'text' : text}, function(data) {
			$('#chat_input').on('keypress.chat', chat);
			prev_chat = data;
			$('#chat').html(data.join(''));
			$("#chat").animate({ scrollTop: $("#chat")[0].scrollHeight });
		}, 'json');
	}	
}

function square_check(square_this, beat) {
	clearInterval(beat);
	row = $(square_this).data('row');
	column = $(square_this).data('column');
	square = $(square_this).data('square');
	// If it hasn't been clicked already, tell the server to count it. 
	if(!$(square_this).hasClass('selected'))
	{
		$(square_this).addClass('selected');
		chosen[row][column] = 1;
		hash = document.URL.split('/').pop();
		action = "move";
	}
	// If it has been, tell the server to clear it.
	else
	{
		$(square_this).removeClass('selected');
		chosen[row][column] = 0;
		action = 'unmove';
	}
	$.post('/game/'+action+'/'+hash, {'square' : square}, function() {
		// Check for a winner
		win_check();
		// Start the heartbeat back up.
		beat = setInterval(heartbeat, 1000);
	});
}
$(document).ready(function() {
	// Start the heartbeat
	heartbeat();
	beat = setInterval(heartbeat, 1000);

	// Add the squares to the board array.
	$('td').each(function() {
		row = $(this).data('row');
		column = $(this).data('column');
		if(chosen[row][column] == 1)
		{
			$(this).addClass('selected');
		}
	});
	// When clicked, clear the heartbeat so it doesn't screw anything up
	$('td').on('click', function() { square_check(this, beat); });
	$('#chat_input').on('keypress.chat', chat);
});
